package com.teravin;

public class StringDataType {
    public static void main(String[] args) {
        String s = "irfan";
        System.out.println("irfan".equals(s));

        StringBuilder builder = new StringBuilder();
        builder.append("hello ").append("world ").append("irfan");
        String a = builder.toString();
        System.out.println(a);

        // ================================

        String[] result = "Hello World Adalah".split(" ");
        for(String k : result){
            System.out.println(k.length());
        }
    }
}
