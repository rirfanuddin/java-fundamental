package com.teravin;

public class Inheritance extends Animal {

    public static void main(String[] args) {
        Inheritance hello = new Inheritance();
        hello.setNama("Kambing");
        hello.setJumlahKaki(4);

        System.out.println("Nama hewan : " + hello.getNama() + " jumlah kaki : " + hello.getJumlahKaki());
        System.out.println("2 x nya adalah : " + hello.kaliDua(hello.getJumlahKaki()));
    }

}

class Animal{
    String nama;
    int jumlahKaki;

    int kaliDua(int jumlahKaki){
        return jumlahKaki*2;
    }

    public String getNama() {
        return nama;
    }

    public void setNama(String nama) {
        this.nama = nama;
    }

    public int getJumlahKaki() {
        return jumlahKaki;
    }

    public void setJumlahKaki(int jumlahKaki) {
        this.jumlahKaki = jumlahKaki;
    }
}
