package com.teravin;

public class ArrayDataType {
    public static void main(String[] args) {
        // declare array with fixed size
        // int[] a = new int[2]; bisa
        // int a[] = new int[2]; bisa

        // declare array with content, if you add more index, it will error
        int a[] = new int[]{1, 2, 3, 4};

//        a[4] = 5;
//        a[5] = 6;

        for(int tmp : a){
            System.out.println(tmp);
        }

        StringBuilder builder = new StringBuilder();
        builder.append("====").append("\n");
        String s = builder.toString();
        System.out.println(s);
        System.out.println(a.length);
    }
}
