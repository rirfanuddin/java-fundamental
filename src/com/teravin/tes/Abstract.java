package com.teravin.tes;

public class Abstract extends Animal {
    public String suara(){
        return "guk guk";
    }
    public static void main(String[] args) {
        Abstract a = new Abstract();
        System.out.println("Hewan yang memiliki suara " + a.suara() + " dan memiliki kaki : "+ a.jumlahKaki());
    }
}

abstract class Animal{
    int jumlahKaki(){
        return 4;
    }
    abstract String suara();
}