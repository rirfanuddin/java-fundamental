package com.teravin;

import java.math.BigDecimal;
import java.math.RoundingMode;

public class BigDecimalDataType {
    public static void main(String[] args) {
        // perbedaan double dan BigDecimal
        double a = 0.02;
        double b = 0.03;
        double result = b-a;
        System.out.println(result);

        BigDecimal c = new BigDecimal("0.102");
        BigDecimal d = new BigDecimal("0.003");

        BigDecimal hasil = d.subtract(c);
        System.out.println(hasil);

        System.out.println("===========");
        BigDecimal e = new BigDecimal("0.12345678901234567890123456789012345678901234567890123456789012345678901234567890");
        BigDecimal f = new BigDecimal("0.0100100");
        BigDecimal g = new BigDecimal("-2.8");
        BigDecimal h = new BigDecimal("1.00");
        BigDecimal i = new BigDecimal("1.0");
        System.out.println(e);

        // operations
        System.out.println("====");
        BigDecimal tambah = d.add(c);
        System.out.println("tambah " + tambah);

        BigDecimal kurang = d.subtract(c);
        System.out.println("kurang " + kurang);

        BigDecimal kali = d.multiply(c);
        System.out.println("kali " + kali);

//        BigDecimal bagi = d.divide(c);
//        System.out.println("bagi : " + bagi);

        // other operation
        System.out.println("precision : " + f.precision());
        System.out.println("scale : " + f.scale());
        System.out.println("signum : " + g.signum());
        System.out.println("compare result : " + h.compareTo(i));

        //rounding
        System.out.println("=========");
        BigDecimal z = new BigDecimal("10.5454");
        z = z.setScale(3, RoundingMode.UP);
        System.out.println("z : " + z);

    }
}
