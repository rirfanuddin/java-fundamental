package com.teravin;

public class Static {
    private static int a;
    private static void sound(){
        System.out.println("hehehe");
    }
    private int jumlahSayap(){
        return 2;
    }
    public static void main(String[] args) {
        a=10;
        System.out.println(a);
        Static.sound();
        Static s = new Static();
        System.out.println(s.jumlahSayap());

        System.out.println("---");

        Static t = new Static();
        t.a = 10;
        System.out.println(t.a);

        Static t2 = new Static();
        t2.a = 15;
        System.out.println(t2.a);
    }
}
