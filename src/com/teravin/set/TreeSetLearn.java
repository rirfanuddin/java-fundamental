package com.teravin.set;

import java.util.Comparator;
import java.util.TreeSet;

public class TreeSetLearn {
    public static void main(String[] args) {
        TreeSet<String> t = new TreeSet<>(Comparator.reverseOrder());
        t.add("aa");
        t.add("cc");
        t.add("dd");
        t.add("mm");
        t.add("pp");
        t.add("rr");

        System.out.println(t);

        // first
//         System.out.println(t);

        // try to clear
//         t.clear();
//         System.out.println(t);

        // try to remove
//         t.remove("aaaa");
//         System.out.println(t);


        // contains()
//        System.out.println(t.contains("ddd"));

        // isEmpty()
//        System.out.println(t.isEmpty());

        // size()
//        System.out.println(t.size());

        // toArray()
//        String[] arr = t.toArray(new String[t.size()]);
//        System.out.println(arr[0]);

        // first() and last()
//        System.out.println(t.first());
//        System.out.println(t.last());

        // higher() and lower()
//        String b = "bbbb";
//        System.out.println(t);
//        System.out.println(t.higher(b));
//        System.out.println(t.lower(b));


    }
}
