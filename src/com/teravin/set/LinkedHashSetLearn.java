package com.teravin.set;

import java.util.LinkedHashSet;

public class LinkedHashSetLearn {
    public static void main(String[] args) {
        LinkedHashSet<String> l = new LinkedHashSet<String>();
        l.add("pp");
        l.add("tt");
        l.add("ff");
        l.add("rr");
        System.out.println(l);
        System.out.println("size : " + l.size());
        System.out.println("contains : " + l.contains("tt"));
        System.out.println("isEmpty : " + l.isEmpty());
        System.out.println("remove : " + l.remove("tt"));
        System.out.println("result : " + l);

        String[] arr = l.toArray(new String[l.size()]);
        System.out.println("arr 0 : " + arr[0]);
        l.clear();
        System.out.println("after clear : " + l);

    }
}
