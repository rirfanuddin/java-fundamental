package id.rirfanuddin.personal;

/**
 * @author rirfanuddin
 */
public class Singleton {
    public static void main(String[] args) {
        JustOne justOne = JustOne.getInstance();
        justOne.demoMethod();

    }
}

class JustOne{
    static JustOne justOne = new JustOne();
    private JustOne(){}
    public static JustOne getInstance(){
        return justOne;
    }
    protected static void demoMethod(){
        System.out.println("Hello world");
    }
}